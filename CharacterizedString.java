import java.util.concurrent.TimeUnit;
import java.util.Scanner;

class CharacterizedString{
    public static void main(String args[]) throws InterruptedException{
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ketik kata/kalimat, lalu tekan enter: ");
        String namaLengkap = scanner.nextLine();
        System.out.println("============================================");
        System.out.println("String penuh: " + namaLengkap);
        System.out.println("============================================");

        System.out.print("Process memecahkan kata/kalimat menjadi karakter");
        int counter = 0;
        while(counter <= 13){
            System.out.print(".");
            Thread.sleep(300);
            counter++;
        }
        System.out.println("\n" + "Done.");

        System.out.println("============================================");
        for (int i = 0; i < namaLengkap.length(); i++){
            char nameChar = namaLengkap.charAt(i);
            System.out.println("Karakter[" + i + "] = " + nameChar);
        }
        System.out.println("============================================");
    }
}